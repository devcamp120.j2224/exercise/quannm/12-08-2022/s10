package com.devcamp.j69s10;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J69s10Application {

	public static void main(String[] args) {
		SpringApplication.run(J69s10Application.class, args);
	}

}
